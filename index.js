const express = require("express");
const app = express();
const mongoose = require("mongoose");

const path = require("path");
const {
    checkTimeMiddleware,
    logUrlMiddleware,
} = require("./apps/middlewares/helper.middleware");
const userRouter = require("./apps/routers/user.router");
const diceHistoryRouter = require("./apps/routers/diceHistory.router");
const prizeRouter = require("./apps/routers/prize.router");
const voucherRouter = require("./apps/routers/voucher.router");
const prizeHistoryRouter = require("./apps/routers/prizeHistory.router");
const voucherHistoryRouter = require("./apps/routers/voucherHistory.router");
const diceRouter = require("./apps/routers/dice.router");
const orderRouter = require("./apps/routers/order.router");

// middleware
app.use(express.json());
app.use(express.static(__dirname + "/views"));
app.use(checkTimeMiddleware);
app.use(logUrlMiddleware);

// db
const connectString = `mongodb://localhost:27017/CRUD_Dice`;
mongoose
    .connect(connectString)
    .then((_) => {
        console.log("Connect MongoDB Successfully");
    })
    .catch((_) => {
        console.log("Connect MongoDB Failure !");
    });

// routers
app.get("/", (req, res) => {
    return res.sendFile(path.join(__dirname, "/views/index.html"));
});

app.get("/random-number", (req, res) => {
    const randDice = Math.floor(Math.random() * 6 + 1);

    return res.json({ randDice });
});

app.use("/users", userRouter);
app.use("/dice-histories", diceHistoryRouter);
app.use("/prizes", prizeRouter);
app.use("/vouchers", voucherRouter);

app.use("/prize-histories", prizeHistoryRouter);
app.use("/voucher-histories", voucherHistoryRouter);
app.use("/devcamp-lucky-dice/dice", diceRouter)
app.use("/orders", orderRouter);


const PORT = 8000;
app.listen(PORT, () => {
    console.log(`listening on ${PORT}`);
});

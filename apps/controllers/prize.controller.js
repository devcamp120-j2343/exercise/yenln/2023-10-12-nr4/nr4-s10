const prizeModel = require("../models/prize.model");
const mongoose = require("mongoose");

const createPrize = async (req, res) => {
    // B1 - collect data
    const { name, description } = req.body;

    if (!name) {
        return res.status(400).json({
            message: "name invalid !",
        });
    }

    try {
        // B3 - save to database
        const result = await prizeModel.create({
            name,
            description,
        });
        return res.status(201).json({
            message: "Create prize successful !",
            data: result,
        });
    } catch (error) {
        if (error.code === 11000) {
            return res.status(400).json({ message: "name already exists !" });
        } else {
            return res.status(500).json({
                message: "Invalid error !",
            });
        }
    }
};

const getAllPrize = async (req, res) => {
    try {
        const result = await prizeModel.find();
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getPrizeById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }

    try {
        const result = await prizeModel.findById(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Couldn't find Prize !",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const updatePrizeById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    const { name, description } = req.body;

    // B2 - validate
    if (name && name == "") {
        return res.status(400).json({
            message: "name invalid !",
        });
    }

    try {
        const result = await prizeModel.findByIdAndUpdate(
            _id,
            { name, description },
            {
                new: true,
            }
        );
        if (result) {
            return res.status(200).json({
                message: "Update prize successful !",
                data: result,
            });
        } else {
            return res.status(404).json({
                message: "Couldn't update prize!",
            });
        }
    } catch (error) {
        if (error.code === 11000) {
            return res.status(400).json({ message: "name already exists !" });
        } else {
            return res.status(500).json({
                message: "Invalid error !",
            });
        }
    }
};

const deletePrizeById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    try {
        const result = await prizeModel.findByIdAndRemove(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Deleta prize successful !" });
        } else {
            return res.status(404).json({
                message: "Couldn't find prize",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

module.exports = {
    createPrize,
    getAllPrize,
    getPrizeById,
    deletePrizeById,
    updatePrizeById,
};

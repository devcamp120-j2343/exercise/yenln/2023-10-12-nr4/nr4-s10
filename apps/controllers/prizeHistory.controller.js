const prizeHistoryModel = require("../models/prizeHistory.model");
const userModel = require("../models/user.model")
const mongoose = require("mongoose");

const createPrizeHistory = async (req, res) => {
    // B1 - collect data
    const { user, prize } = req.body;

    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "user invalid !",
        });
    }
    if (!mongoose.Types.ObjectId.isValid(prize)) {
        return res.status(400).json({
            message: "prize invalid !",
        });
    }

    try {
        // B3 - save to database
        const result = await prizeHistoryModel.create({
            user,
            prize,
        });
        return res.status(201).json({
            message: "Create PrizeHistory successful !",
            data: result,
        });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getAllPrizeHistory = async (req, res) => {
    const user = req.query.user;
    console.log("user:::::::::::::::", user);

    let condition = {};
    if (user && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "user invalid !",
        });
    }

    if (user) condition.user = user;
    try {
        const result = await prizeHistoryModel.find(condition);
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getPrizeHistoryById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }

    try {
        const result = await prizeHistoryModel.findById(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Couldn't find PrizeHistory !",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const updatePrizeHistoryById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    const { user, prize } = req.body;

    // B2 - validate
    if (user && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "user invalid !",
        });
    }
    if (prize && !mongoose.Types.ObjectId.isValid(prize)) {
        return res.status(400).json({
            message: "prize invalid !",
        });
    }

    try {
        const result = await prizeHistoryModel.findByIdAndUpdate(
            _id,
            { user, prize },
            {
                new: true,
            }
        );
        if (result) {
            return res.status(200).json({
                message: "Update PrizeHistory successful !",
                data: result,
            });
        } else {
            return res.status(404).json({
                message: "Couldn't update PrizeHistory!",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const deletePrizeHistoryById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    try {
        const result = await prizeHistoryModel.findByIdAndRemove(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Deleta PrizeHistory successful !" });
        } else {
            return res.status(404).json({
                message: "Couldn't find PrizeHistory",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getPrizeHistoryByUserName = async (req, res) => {
    const username = req.query.username;
    if (!username) return res.status(400).json({ message: "Username invalid" });

    const user = await userModel.findOne({username });
    if(!user) return res.status(200).json([]);

    const prizeHistory = await prizeHistoryModel.find({user: user._id});
    if (!prizeHistory) return res.status(400).json({ message: "Bad request !" });

    return res.status(200).json(prizeHistory);

}

module.exports = {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    deletePrizeHistoryById,
    updatePrizeHistoryById,
    getPrizeHistoryByUserName
};

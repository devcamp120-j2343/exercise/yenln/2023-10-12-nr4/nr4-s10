const diceHistoryModel = require("../models/diceHistory.model");
const userModel = require("../models/user.model");

const mongoose = require("mongoose");

const createDiceHistory = async (req, res) => {
    // B1 - collect data
    const { user } = req.body;

    // B2 - validate
    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "User id invalid !",
        });
    }

    try {
        const { randDice } = await getRandomDice(
            "http://localhost:8000/random-number"
        );
        // B3 - save to database
        const result = await diceHistoryModel.create({
            user,
            dice: randDice,
        });
        return res.status(201).json({
            message: "Create dice history successful !",
            data: result,
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getAllDiceHistory = async (req, res) => {
    const user = req.query.user;
    console.log("user:::::::::::::::", user);

    let condition = {};
    if (user && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "user invalid !",
        });
    }

    if (user) condition.user = user;

    try {
        const result = await diceHistoryModel.find(condition);
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getDiceHistoryById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }

    try {
        const result = await diceHistoryModel.findById(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Couldn't find DiceHistory !",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const updateDiceHistoryById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    const { user } = req.body;

    // B2 - validate
    if (user && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "User id invalid !",
        });
    }

    try {
        const result = await diceHistoryModel.findByIdAndUpdate(
            _id,
            { user },
            {
                new: true,
            }
        );
        if (result) {
            return res.status(200).json({
                message: "Update dice history successful !",
                data: result,
            });
        } else {
            return res.status(404).json({
                message: "Couldn't update dice history!",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const deleteDiceHistoryById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    try {
        const result = await diceHistoryModel.findByIdAndRemove(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Deleta dice history successful !" });
        } else {
            return res.status(404).json({
                message: "Couldn't find dice history",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getDiceHistoryByUserName = async (req, res) => {
    const username = req.query.username; 
    if(!username) return res.status(400).json({message:`username is invalid`});

    const user = await userModel.findOne({username});
    if (!user) return res.status(200).json([]);

    const diceHistory = await diceHistoryModel.find({user: user._id });
    if (!diceHistory) return res.status(400).json({ message: "Bad request !" });

    return res.status(200).json(diceHistory);

}


const getRandomDice = async (url) => {
    return (await fetch(url)).json();
};

module.exports = {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    deleteDiceHistoryById,
    updateDiceHistoryById,
    getDiceHistoryByUserName
};

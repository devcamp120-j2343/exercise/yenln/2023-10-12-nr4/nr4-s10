const userModel = require("../models/user.model");
const mongoose = require("mongoose");

const createUser = async (req, res) => {
    // B1 - collect data
    const { username, firstname, lastname } = req.body;

    // B2 - validate
    if (!username) {
        return res.status(400).json({
            message: "username invalid !",
        });
    }
    if (!firstname) {
        return res.status(400).json({
            message: "firstname invalid !",
        });
    }
    if (!lastname) {
        return res.status(400).json({
            message: "lastname invalid !",
        });
    }

    try {
        // B3 - save to database
        const result = await userModel.create({
            username,
            firstname,
            lastname,
        });
        return res
            .status(201)
            .json({ message: "Create user successful !", data: result });
    } catch (error) {
        if (error.code === 11000) {
            return res
                .status(400)
                .json({ message: "username already exists !" });
        } else {
            return res.status(500).json({
                message: "Invalid error !",
            });
        }
    }
};

const getAllUser = async (req, res) => {
    console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    try {
        const result = await userModel.find();
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getUserById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }

    try {
        const result = await userModel.findById(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Couldn't find user !",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const updateUserById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    const { username, firstname, lastname } = req.body;

    // B2 - validate
    if (username && username == "") {
        return res.status(400).json({
            message: "username invalid !",
        });
    }
    if (firstname && firstname == "") {
        return res.status(400).json({
            message: "firstname invalid !",
        });
    }
    if (lastname && lastname == "") {
        return res.status(400).json({
            message: "lastname invalid !",
        });
    }

    try {
        let updateUser = {};
        if (username) {
            updateUser.username = username;
        }
        if (firstname) {
            updateUser.firstname = firstname;
        }
        if (lastname) {
            updateUser.lastname = lastname;
        }

        const result = await userModel.findByIdAndUpdate(_id, updateUser, {
            new: true,
        });
        if (result) {
            return res
                .status(200)
                .json({ message: "Update user successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Couldn't update user!",
            });
        }
    } catch (error) {
        if (error.code === 11000) {
            return res
                .status(400)
                .json({ message: "username already exists !" });
        } else {
            return res.status(500).json({
                message: "Invalid error !",
            });
        }
    }
};

const deleteUserById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    try {
        const result = await userModel.findByIdAndRemove(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Deleta user successful !" });
        } else {
            return res.status(404).json({
                message: "Couldn't find User",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    deleteUserById,
    updateUserById,
};

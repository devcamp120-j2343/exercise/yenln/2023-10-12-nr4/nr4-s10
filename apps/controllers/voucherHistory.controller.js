const voucherHistoryModel = require("../models/voucherHistory.model");
const userModel = require("../models/user.model");

const mongoose = require("mongoose");

const createVoucherHistory = async (req, res) => {
    // B1 - collect data
    const { user, voucher } = req.body;

    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "user invalid !",
        });
    }
    if (!mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            message: "voucher invalid !",
        });
    }

    try {
        // B3 - save to database
        const result = await voucherHistoryModel.create({
            user,
            voucher,
        });
        return res.status(201).json({
            message: "Create VoucherHistory successful !",
            data: result,
        });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getAllVoucherHistory = async (req, res) => {
    const user = req.query.user;
    console.log("user:::::::::::::::", user);

    let condition = {};
    if (user && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "user invalid !",
        });
    }
    if (user) condition.user = user;

    try {
        const result = await voucherHistoryModel.find(condition);
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getVoucherHistoryById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }

    try {
        const result = await voucherHistoryModel.findById(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Couldn't find VoucherHistory !",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const updateVoucherHistoryById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    const { user, voucher } = req.body;

    // B2 - validate
    if (user && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "user invalid !",
        });
    }
    if (voucher && !mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            message: "voucher invalid !",
        });
    }

    try {
        const result = await voucherHistoryModel.findByIdAndUpdate(
            _id,
            { user, voucher },
            {
                new: true,
            }
        );
        if (result) {
            return res.status(200).json({
                message: "Update VoucherHistory successful !",
                data: result,
            });
        } else {
            return res.status(404).json({
                message: "Couldn't update VoucherHistory!",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const deleteVoucherHistoryById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    try {
        const result = await voucherHistoryModel.findByIdAndRemove(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Deleta VoucherHistory successful !" });
        } else {
            return res.status(404).json({
                message: "Couldn't find VoucherHistory",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getVoucherHistoryByUsername = async (req,res) => {
    const username = req.query.username;
    if (!username) return res.status(400).json({ message: "Username invalid" });

    const user = await userModel.findOne({ username });
    if (!user) return res.status(200).json([]);

    const voucherHistoryModel = await voucherHistoryModel.find({ user: user._id });
    if (!voucherHistoryModel) return res.status(400).json({ message: "Bad request !" });

    return res.status(200).json(voucherHistoryModel);

}

module.exports = {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    deleteVoucherHistoryById,
    updateVoucherHistoryById,
    getVoucherHistoryByUsername
};

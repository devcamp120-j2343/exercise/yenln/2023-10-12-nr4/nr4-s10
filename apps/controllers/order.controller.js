const orderModel = require("../models/order.model");
const userModel = require("../models/user.model");

const mongoose = require("mongoose");

const createOrder = async (req, res) => {
    // B1 - collect data
    const {
        orderCode,
        email,
        username,
        firstname,
        lastname } = req.body;

    // B2 - validate
    try {
        const emailFound = await userModel.findOne({ email })

        if (!emailFound) {
            const newUserInfor = {
                username,
                firstname,
                lastname,
                email
            }

            const newOrderInfor = {
                newUserInfor,
                orderCode
            }
            const newUserCreated = await userModel.create(newUserInfor);
            const newOrderCreated = await orderModel.create(newOrderInfor);
            return res.status(200).json({
                message: `Successfully create `,
                data: newOrderCreated
            })
        }
        const newOrderInfor = { orderCode };
        const newOrderCreated = await orderModel.create(newOrderInfor);
        return res.status(200).json({
            message: `Successfully create `,
            data: newOrderCreated
        })

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};


module.exports = {
    createOrder,
    // getAllOrder,
    // getOrderById,
    // deleteOrderById,
    // updateOrderById,
    // getOrderByUserName
};

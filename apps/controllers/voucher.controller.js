const voucherModel = require("../models/voucher.model");
const mongoose = require("mongoose");

const createVoucher = async (req, res) => {
    // B1 - collect data
    const { code, discount, note } = req.body;

    if (!code) {
        return res.status(400).json({
            message: "code invalid !",
        });
    }
    if (!discount) {
        return res.status(400).json({
            message: "discount invalid !",
        });
    }

    if (isNaN(discount)) {
        return res.status(400).json({
            message: "discount must be number !",
        });
    }

    try {
        // B3 - save to database
        const result = await voucherModel.create({
            code,
            discount,
            note,
        });
        return res.status(201).json({
            message: "Create Voucher successful !",
            data: result,
        });
    } catch (error) {
        if (error.code === 11000) {
            return res.status(400).json({ message: "code already exists !" });
        } else {
            return res.status(500).json({
                message: "Invalid error !",
            });
        }
    }
};

const getAllVoucher = async (req, res) => {
    try {
        const result = await voucherModel.find();
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getVoucherById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }

    try {
        const result = await voucherModel.findById(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Couldn't find voucher !",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const updateVoucherById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    const { code, discount, note } = req.body;

    // B2 - validate
    if (code && code == "") {
        return res.status(400).json({
            message: "code invalid !",
        });
    }
    if (discount && isNaN(discount)) {
        return res.status(400).json({
            message: "discount invalid, must be number !",
        });
    }

    let updateVoucher = {};
    if (code) {
        updateVoucher.code = code;
    }
    if (discount) {
        updateVoucher.discount = discount;
    }
    if (note) {
        updateVoucher.note = note;
    }

    try {
        const result = await voucherModel.findByIdAndUpdate(
            _id,
            updateVoucher,
            {
                new: true,
            }
        );
        if (result) {
            return res.status(200).json({
                message: "Update Voucher successful !",
                data: result,
            });
        } else {
            return res.status(404).json({
                message: "Couldn't update Voucher!",
            });
        }
    } catch (error) {
        if (error.code === 11000) {
            return res.status(400).json({ message: "code already exists !" });
        } else {
            return res.status(500).json({
                message: "Invalid error !",
            });
        }
    }
};

const deleteVoucherById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    try {
        const result = await voucherModel.findByIdAndRemove(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Deleta Voucher successful !" });
        } else {
            return res.status(404).json({
                message: "Couldn't find Voucher",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};


module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    deleteVoucherById,
    updateVoucherById,
};

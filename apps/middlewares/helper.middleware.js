const checkTimeMiddleware = (req, res, next) => {
    let date = new Date();
    let message = `Now is ${date.getDate()}/${
        date.getMonth() + 1
    }/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;

    console.log(message);
    next();
};

const logUrlMiddleware = (req, res, next) => {
    console.log(`Request URL: ${req.originalUrl}`);
    next();
};

module.exports = { checkTimeMiddleware, logUrlMiddleware };

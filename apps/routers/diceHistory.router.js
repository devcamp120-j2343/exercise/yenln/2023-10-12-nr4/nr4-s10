const express = require("express");
const router = express.Router();

const {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    deleteDiceHistoryById,
    updateDiceHistoryById,
    getDiceHistoryByUserName
} = require("../controllers/diceHistory.controller");

router.get("/", getAllDiceHistory, getDiceHistoryByUserName);

router.get("/:id", getDiceHistoryById);

router.post("/", createDiceHistory);

router.put("/:id", updateDiceHistoryById);

router.delete("/:id", deleteDiceHistoryById);

module.exports = router;

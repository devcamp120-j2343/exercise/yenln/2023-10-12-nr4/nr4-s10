const express = require("express");
const router = express.Router();

const diceController = require("../controllers/diceController (1)");

// router.get("/", diceController.getAllDice);

// router.get("/:id", diceController.getDiceById);

router.post("/", diceController.createDice);

// router.put("/:id", diceController.updateDiceById);

// router.delete("/:id", diceController.deleteDiceById);

module.exports = router;

const express = require("express");
const router = express.Router();

const {
    createPrize,
    getAllPrize,
    getPrizeById,
    deletePrizeById,
    updatePrizeById,
} = require("../controllers/prize.controller");

router.get("/", getAllPrize);

router.get("/:id", getPrizeById);

router.post("/", createPrize);

router.put("/:id", updatePrizeById);

router.delete("/:id", deletePrizeById);

module.exports = router;

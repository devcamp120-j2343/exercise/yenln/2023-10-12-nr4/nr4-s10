const express = require("express");
const router = express.Router();

const {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    deleteVoucherHistoryById,
    updateVoucherHistoryById,
} = require("../controllers/voucherHistory.controller");

router.get("/", getAllVoucherHistory);

router.get("/:id", getVoucherHistoryById);

router.post("/", createVoucherHistory);

router.put("/:id", updateVoucherHistoryById);

router.delete("/:id", deleteVoucherHistoryById);

module.exports = router;

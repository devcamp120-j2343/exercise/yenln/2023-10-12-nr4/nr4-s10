const express = require("express");
const router = express.Router();

const {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    deletePrizeHistoryById,
    updatePrizeHistoryById,
} = require("../controllers/prizeHistory.controller");

router.get("/", getAllPrizeHistory);

router.get("/:id", getPrizeHistoryById);

router.post("/", createPrizeHistory);

router.put("/:id", updatePrizeHistoryById);

router.delete("/:id", deletePrizeHistoryById);

module.exports = router;

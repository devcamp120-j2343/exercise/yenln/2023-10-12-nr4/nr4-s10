const express = require("express");
const router = express.Router();

const orderController = require("../controllers/order.controller");

// router.get("/", orderController.getAllorder);

// router.get("/:id", orderController.getorderById);

router.post("/", orderController.createOrder);

// router.put("/:id", orderController.updateorderById);

// router.delete("/:id", orderController.deleteorderById);

module.exports = router;

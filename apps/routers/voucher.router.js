const express = require("express");
const router = express.Router();

const {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    deleteVoucherById,
    updateVoucherById,
} = require("../controllers/voucher.controller");

router.get("/", getAllVoucher);

router.get("/:id", getVoucherById);

router.post("/", createVoucher);

router.put("/:id", updateVoucherById);

router.delete("/:id", deleteVoucherById);

module.exports = router;

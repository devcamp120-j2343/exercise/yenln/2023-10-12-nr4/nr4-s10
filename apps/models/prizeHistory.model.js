const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const prizeHistorySchema = new Schema(
    {
        user: {
            type: Schema.Types.ObjectId,
            ref: "User",
            require: true,
        },
        prize: {
            type: Schema.Types.ObjectId,
            ref: "Prize",
            require: true,
        },
        createdAt: {
            type: Date,
            default: Date.now()
        },
        updatedAt: {
            type: Date,
            default: Date.now()
        }
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("PrizeHistory", prizeHistorySchema);

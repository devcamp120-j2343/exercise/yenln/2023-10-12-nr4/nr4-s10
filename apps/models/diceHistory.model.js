const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const diceHistorySchema = new Schema(
    {
        user: {
            type: Schema.Types.ObjectId,
            ref: "User",
            require: true,
        },
        dice: {
            type: Number,
            require: true,
        },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("DiceHistory", diceHistorySchema);

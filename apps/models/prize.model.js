const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const prizeSchema = new Schema(
    {
        name: {
            type: String,
            require: true,
            unique: true,
        },
        description: String,
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("Prize", prizeSchema);

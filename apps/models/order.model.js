const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const orderSchema = new Schema(
    {
        orderCode: {
            type: Schema.Types.ObjectId,
        },
        user:[
            {
                type: Schema.Types.ObjectId,
                ref: "User"
            }
        ],
        email: {
            type: String,
            require: true,
        },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("order", orderSchema);

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const voucherHistorySchema = new Schema(
    {
        user: {
            type: Schema.Types.ObjectId,
            ref: "User",
            require: true,
        },
        voucher: {
            type: Schema.Types.ObjectId,
            ref: "Voucher",
            require: true,
        },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("VoucherHistory", voucherHistorySchema);

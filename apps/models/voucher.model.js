const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const voucherSchema = new Schema(
    {
        code: {
            type: String,
            require: true,
            unique: true,
        },
        discount: {
            type: Number,
            require: true,
        },
        note: String,
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("Voucher", voucherSchema);
